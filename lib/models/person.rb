class Person

  attr_accessor :attribute_a
  attr_accessor :attribute_b
  attr_accessor :attribute_c
  attr_accessor :attribute_d

  # this method should raise an issue in code legacy
  def x(y=nil)
    if y
      "Testing code styling violations"
    end
  end

  def method_looks_correct(parameter_name = 'with_correct_format')
    puts parameter_name
  end

end