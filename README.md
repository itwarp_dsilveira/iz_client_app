# Install #

## Clone

```
git clone git@bitbucket.org:itwarp_dsilveira/iz_client_app.git
```

## Install Gems

```
bundle install
```

## try to run ovs_tests

```
be ovs_tests --help
```

## Notes

I've used this version of Ruby `1.9.3-p392` since it's the version used in almost all ruby projects.
But, this thrown an error during `bundle install`:

```
Installing rack 2.0.1
Gem::InstallError: rack requires Ruby version >= 2.2.2.
Installing json 2.0.2 with native extensions
Gem::InstallError: json requires Ruby version ~> 2.0.
```

So it went away once I indicated the version of rack to `~> 1.6.4`

That's all.